#[cfg(test)]
mod tests {
    use std::thread;
    use std::time::Duration;

    #[test]
    fn it_works() {
        // #big_test_suite
        println!("Sleeping");
        thread::sleep(Duration::new(180, 0));
        println!("Woke up");
        assert_eq!(2 + 2, 4);
        assert_ne!(1, 2);
    }

    #[test]
    fn it_doesnt_work() {
        assert!(true);
    }
}
